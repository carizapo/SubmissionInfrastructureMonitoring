#!/usr/bin/python
import sys,os
import htcondor,classad

# Arguments are a list of central managers of pools
CentralManagerMachines=sys.argv[1:]
if len(CentralManagerMachines) == 0 :
  CentralManagerMachines=["cmsgwms-collector-global.cern.ch",
    "cmsgwms-collector-tier0.cern.ch","vocms0840.cern.ch",
    "cmsgwms-collector-itb.cern.ch","vocms0804.cern.ch"]

# Get Machine names and information at CERN

projection=["Name","CollectorHost", "Machine","CondorPlatform",
  "CondorVersion","MyAddress","MaxJobsRunning"]

print ("%-50s" % 'CENTRAL MANAGERS, CCB, AND SCHEDD MACHINES:' )
print
print ("%20s," % 'Machine' ),
print ("%20s," % 'IPv4address' ),
print ("%15s," % 'AvailabilityZone' ),
print ("%15s," % 'CondorVersion' ),
print ("%15s," % 'CollectorHost' ),
print ("%15s," % 'MaxJobsRunning')

Names=[]
Factories=[]
for CentralManagerMachine in CentralManagerMachines :
  Collector=htcondor.Collector(CentralManagerMachine)
  Collector9620=htcondor.Collector(CentralManagerMachine+":9620")

  # CERN Factory : discoverable with GLIDECLIENT_ReqNode from pilot classAd
  pilot_projection=["GLIDECLIENT_ReqNode"]
  startd_ads=Collector.query(htcondor.AdTypes.Startd,"true",pilot_projection)
  for ad in startd_ads :
    try :
      if str(ad['GLIDECLIENT_ReqNode']) not in Factories :
        Factories.append(ad['GLIDECLIENT_ReqNode'])  
    except KeyError :
      continue

  # Central manager, CCB, and schedd ads from collector
  central_manager_ads=Collector.query(htcondor.AdTypes.Collector,
    'true',projection)
  schedd_ads=Collector9620.query(htcondor.AdTypes.Schedd,
    'true',projection)
  for ad in central_manager_ads+schedd_ads :
    if ad['Name'] not in Names and "cern.ch" in ad['Machine'] :
      Names.append(ad['Name']) 
      Machine=ad['Machine']
      print ("%20s," % Machine ),
      IPv4address = str(ad['MyAddress']).split(":")[0].split("<")[1]
      print ("%20s," % IPv4address ),
      Command='ssh -o "StrictHostKeyChecking=no" '+Machine+' grep Availability /etc/motd'
      AvailabilityZone=os.popen(Command).read()
      print ("%15s," % AvailabilityZone.split()[-1] ),
      CondorVersion=str(ad['CondorVersion']).split()[1]
      print ("%15s," % CondorVersion ),
      try : 
        CollectorHost=ad['CollectorHost'].split(".")[0]
        print ("%15s," % CollectorHost ),
        print ("%15d" % ad['MaxJobsRunning'] )
      except KeyError :
        print

# Other Machines at CERN we get by Name

def discover_machine(Machine) :
  Command='ssh -o "StrictHostKeyChecking=no" '+Machine+' grep Availability /etc/motd'
  AvailabilityZone=str(os.popen(Command).read()).split()[-1]
  Command='host '+Machine
  IPv4Address=str(os.popen(Command).read()).split()[-1]
  Command='ssh -o "StrictHostKeyChecking=no" '+Machine+' condor_version | head -1'
  CondorVersion=str(os.popen(Command).read()).split()[1]
  print ("%20s," % Machine),
  print ("%20s," % IPv4Address),
  print ("%15s," % AvailabilityZone),
  print ("%15s," % CondorVersion)
  return

#Monitoring Node
discover_machine("vocms0851.cern.ch")

# Frontends: 
discover_machine("cmsgwms-frontend-global.cern.ch")
discover_machine("cmsgwms-frontend-tier0.cern.ch")
discover_machine("cmsgwms-frontend-itb.cern.ch")
discover_machine("vocms0801.cern.ch") # ITB dev FE

# Factories
for Factory in Factories :
  if "cern.ch" in str(Factory) :
    discover_machine(Factory)

sys.exit()