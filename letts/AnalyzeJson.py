#!/usr/bin/python
import sys
import htcondor
import classad
import json
import time
import datetime
import glob
import os

try :
  INPUTFILE=sys.argv[1]
except IndexError :
  INPUTFILE=max(glob.glob('./*.js'),key=os.path.getctime)

# Verify that the input file is parsable json
try :
  json_data=open(INPUTFILE,"r")
except IOError :
  print >> sys.stderr, "Unable to open file: "+INPUTFILE
  sys.exit(4)
try :
  data = json.load(json_data)
  print >> sys.stderr, "json input verified: "+INPUTFILE
except ValueError :
  print >> sys.stderr, "Unable to decode json file: "+INPUTFILE
  sys.exit(5)
json_data.close()


print "<HTML>"
print "<BODY>"
print "<PRE>"

os.environ["TZ"]="GMT"
EpochTime=int(str(INPUTFILE).split(".")[-2])
print "HTCondor Pool Monitor",
print time.strftime('%Y-%m-%d %H:%M:%S %Z', time.gmtime(EpochTime))

json_data=open(INPUTFILE,"r")
data = json.load(json_data)

# autocluster Ads
AutoClusters={}
JobPressure={}
for datum in data :
  try :
    Type=datum["MyType"]
  except KeyError :
    # AutoCluster ads don't define MyType or Schedd name - fix !
    try :
      DESIRED_Sites=str(datum["DESIRED_Sites"])
      try :
        Pressure=int(datum["JobCount"])*int(datum["RequestCpus"])
      except ValueError :
        Pressure=int(datum["JobCount"])*int(datum["OriginalCpus"])
      for Site in DESIRED_Sites.split(",") :
        try :
          AutoClusters[Site]+=1
        except KeyError :
          AutoClusters[Site]=1
        try :
          JobPressure[Site]+=Pressure
        except KeyError :
          JobPressure[Site]=Pressure
    except KeyError :
      continue

# Negotiator Ads
print("\n%-31s,%10s" % ("Negotiator","Cycle (s)"))
NEGO={}
for datum in data :
  try :
    Type=datum["MyType"]
    if Type !=  "Negotiator" : continue
  except KeyError :
    continue
  try : 
    NEGO[datum["Name"]]=datum["LastNegotiationCycleDuration0"]
  except KeyError :
    continue
for nego in sorted(NEGO.keys()) :
  print("%-31s,%10i" % (nego, NEGO[nego]))


print("\n%-31s,%10s,%10s,%10s,%10s,%10s" % (
  "Schedd Name", "MaxJobs", "RunJobs", "IdleJobs", "HeldJobs", "DutyCycle"))
SCHEDD={}
for datum in data :
  try :
    Type=datum["MyType"]
    if Type !=  "Scheduler" : continue
  except KeyError :
    continue
  try : 
    SCHEDD[datum["Name"]]=[
      datum["MaxJobsRunning"],
      datum["TotalRunningJobs"],
      datum["TotalIdleJobs"],
      datum["TotalHeldJobs"],
      datum["RecentDaemonCoreDutyCycle"]]
  except KeyError :
    continue
for schedd in sorted(SCHEDD.keys()) :
  print("%-31s,%10i,%10i,%10i,%10i,%9i%%" % (
    schedd, 
    SCHEDD[schedd][0], 
    SCHEDD[schedd][1], 
    SCHEDD[schedd][2], 
    SCHEDD[schedd][3],
    100.*float(SCHEDD[schedd][4])))

Analysis={}
Production={}
Unclaimed={}
Idle={}
Retire={}
Starve={}
for datum in data :
  try :
    Type=datum["MyType"]
    if Type !=  "Machine" : continue
  except KeyError :
    continue
  try :
    Cpus=datum["CPUs"]
    if Cpus == 0 : continue
  except KeyError :
    continue
  try :
    State=datum["State"]
    Activity=datum["Activity"]
    Site=datum["GLIDEIN_CMSSite"]
  except KeyError :
    continue
  if State == "Unclaimed" :
    try :
      GLIDEIN_ToRetire=datum["GLIDEIN_ToRetire"]
    except KeyError :
      GLIDEIN_ToRetire=datum["MyCurrentTime"]+1
    if GLIDEIN_ToRetire < datum["MyCurrentTime"] :
      try :
        Retire[Site]+=Cpus 
      except KeyError :
        Retire[Site]=Cpus
    elif datum["Memory"] < 2048*Cpus :
      try :
        Starve[Site]+=Cpus 
      except KeyError :
        Starve[Site]=Cpus
    else :
      try :
        Unclaimed[Site]+=Cpus 
      except KeyError :
        Unclaimed[Site]=Cpus
  else :
    try :
      JobId=datum["GlobalJobId"]
      if "crab" in JobId or "login.uscms.org" in JobId : 
        try :
          Analysis[Site]+=Cpus
        except KeyError :
          Analysis[Site]=Cpus
      # add Tier-0 workflows here
      else :
        try :
          Production[Site]+=Cpus
        except KeyError :
          Production[Site]=Cpus
    except KeyError :
      continue


Keys=list(set(Analysis.keys()+Production.keys()))
prod0=0;prod1=0;prod2=0;prod3=0;prodt1=0;prodt2=0
anal0=0;anal1=0;anal2=0;anal3=0;analt1=0;analt2=0
retir=0;starv=0;idlec=0

nbins=20
histo=[0]*(nbins+1)

print("\n%-31s,%10s,%10s,%10s,%10s,%10s,%10s,%10s,%10s,%10s" % 
  ("Site", "AnaCpus", "ProdCpus", "AnaShare", "RetireCpus", 
  "StarveCpus", "IdleCpus", "SchedEff", "Pressure", "AutoClusts"))
for Site in sorted(Keys): 
  try :
    AnalCpus=Analysis[Site]
  except KeyError :
    AnalCpus=0
  try :
    ProdCpus=Production[Site]
  except KeyError :
    ProdCpus=0
  try :
    RetireCpus=Retire[Site]
  except KeyError :
    RetireCpus=0
  try :
    StarveCpus=Starve[Site]
  except KeyError :
    StarveCpus=0
  try :
    IdleCpus=Unclaimed[Site]
  except KeyError :
    IdleCpus=0
  try :
    PresCpus=JobPressure[Site]
  except KeyError :
    PresCpus=0
  try :
    AutoClus=AutoClusters[Site]
  except KeyError :
    AutoClus=0
  try :
    SchedEff=100.*IdleCpus/(AnalCpus+ProdCpus+RetireCpus+StarveCpus+IdleCpus)
    SchedEff=100-SchedEff
  except ZeroDivisionError :
    SchedEff=0.
  try :
    Pct=100.*AnalCpus/(AnalCpus+ProdCpus)
    print("%-31s,%10d,%10d,%9.1f%%,%10d,%10d,%10d,%9.1f%%,%10d,%10d" % 
      (Site, AnalCpus, ProdCpus, Pct, RetireCpus,
      StarveCpus, IdleCpus, SchedEff, PresCpus, AutoClus))
    if "T2" in Site and "T2_US" not in Site and "CERN" not in Site and AnalCpus+ProdCpus > 0 :
      bin=int(nbins*Pct/100.)
      histo[bin]+=1 
  except ZeroDivisionError :
    Pct="N/A"
    print("%-31s,%10d,%10d,%10s,%10d,%10d,%10d,%10d,%10d" % 
      (Site, AnalCpus, ProdCpus, Pct, RetireCpus,
      StarveCpus, IdleCpus, PresCpus, AutoClus))

  prod0+=ProdCpus
  anal0+=AnalCpus
  retir+=RetireCpus
  starv+=StarveCpus
  idlec+=IdleCpus
  if "T1" in Site or "HLT" in Site :
    prod1+=ProdCpus
    anal1+=AnalCpus
  elif "T2_US" in Site :
    prod2+=ProdCpus
    anal2+=AnalCpus
  elif "CERN" not in Site :
    prod3+=ProdCpus
    anal3+=AnalCpus

  if "T1" in Site :
    prodt1+=ProdCpus
    analt1+=AnalCpus
  elif "T2" in Site and not "CERN" in Site :
    prodt2+=ProdCpus
    analt2+=AnalCpus


try :
  print("%-31s,%10d,%10d,%9.1f%%," % 
    ("TOTAL", anal0, prod0, 100.*anal0/(prod0+anal0))),
  effic=100.-(100.*idlec/(prod0+anal0++retir+starv+idlec))
  print("%9d,%10d,%10d,%9.1f%%" % (retir,starv,idlec,effic) )
  print
  print("%-31s,%10d,%10d,%9.1f%%" % 
    ("TOTAL Nego. T1&HLT", anal1, prod1, 100.*anal1/(prod1+anal1)))
  print("%-31s,%10d,%10d,%9.1f%%" % 
    ("TOTAL Nego. T2_US ", anal2, prod2, 100.*anal2/(prod2+anal2)))
  print("%-31s,%10d,%10d,%9.1f%%" % 
    ("TOTAL Nego. Other ", anal3, prod3, 100.*anal3/(prod3+anal3)))
  print
  print("%-31s,%10d,%10d,%9.1f%%" % 
    ("TOTAL T1", analt1, prodt1, 100.*analt1/(prodt1+analt1)))
  print("%-31s,%10d,%10d,%9.1f%%" % 
    ("TOTAL T2", analt2, prodt2, 100.*analt2/(prodt2+analt2)))
  print
  print "Histogram of Non-US T2 Analysis Percentage:"
  print histo
except ZeroDivisionError :
  print

# print time difference
print "</PRE>"
print "</BODY>"
print "</HTML>"


sys.exit()
