This is a set of simple monitoring scripts that run queries to the CMS HTCondor global pool, then plot results via google charts scripts in HTML files

/queries: query shell scripts, plus a few python ones (calculate averages, parse gWMS FE xml, etc)

/make_html: scripts that build the html files that present the plots with the accumulated data

/out: contains the data on the metrics collected via the queries

/HTML: destination directory for html files from which they are copied to EOS

Note that this monitoring pages are basically in constant evolution
