#!/bin/sh
# Get collector properties from CMS global pool
# Antonio Perez-Calero Yzquierdo June 2018

source /data/srv/aperezca/Monitoring/env_itb.sh

collector=$($WORKDIR/collector_itb.sh)

#echo $collector

date_all=`date -u +%s`


data_global_pool=$(condor_status -pool $collector -const 'regexp("'$collector'", Name)' -collector -af ActiveQueryWorkers PendingQueries RecentDroppedQueries RecentDaemonCoreDutyCycle RecentForkQueriesFromNEGOTIATOR RecentForkQueriesFromTOOL RecentUpdatesTotal RecentUpdatesLost SubmitterAds)


echo $date_all $data_global_pool

echo $date_all $data_global_pool >>$OUTDIR/collector_itb_pool

