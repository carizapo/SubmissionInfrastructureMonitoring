#!/bin/bash

# cd into the directory where the script is so we can use relative paths
script_path=$(dirname $(readlink -f $0))
cd $script_path

# Pick up the common StompAMQ module
CMSMonitoring="/data/srv/SubmissionInfrastructureMonitoring/CMSMonitoring/src/python"
if [ -z $PYTHONPATH ] ; then
  export PYTHONPATH=$CMSMonitoring
else
  export PYTHONPATH=${PYTHONPATH}:$CMSMonitoring
fi

# Monitor the itb pool
python my_monitor.py itb push 1>> log/itb/log.out 2>> log/itb/log.err
# Monitor the global pool
python my_monitor.py global push 1>> log/global/log.out 2>> log/global/log.err
# Monitor the cern pool
python my_monitor.py cern push 1>> log/cern/log.out 2>> log/cern/log.err
# Monitor the volunteer pool
python my_monitor.py volunteer push 1>> log/volunteer/log.out 2>> log/volunteer/log.err

